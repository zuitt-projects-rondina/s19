/*alert("Hello");*/

/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
	
*/


let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
};

let student2 = {
	name : "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
};

const introduce = () => {

	//Note: You can destructure objects inside functions.
	const {name, birthday, age, isEnrolled,classes} = student1;


	console.log(`Hi! I'm ${name} I am ${age} years old.`);
	console.log(`I study the following courses ${classes}`);
}

introduce();



const getCube = (num) => num ** 3;
let cube = getCube(3);
console.log(cube)



let numArr = [15,16,32,21,21,2]

numArr.forEach((num) =>{

	console.log(num);
})

let numsSquared = numArr.map((num)=>{

	return num ** 2;

})

console.log(numsSquared);


/*2. Create a class constructor able to receive 3 arguments
		-It should be able to receive two strings and a number
		-Using the this keyword assign properties:
			name, 
			breed, 
			dogAge = <7x human years> 
			-assign the parameters as values to each property.

	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.
*/

class Dog{
	constructor(name, breed, dogAge){
			this.name = name;
			this.breed = breed;
			this.age =  dogAge / 7  ;

	}

};

let dog1 = new Dog("Una", "Shitsu", 14);
console.log(dog1);
let dog2 = new Dog("Veyo", "Aspin", 21);
console.log(dog2);

