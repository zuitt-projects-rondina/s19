/*alert("Hello");*/

// ES6 Updates
	//EcmaScript6 or ES8 is the new version of javascript
	//Javascript is formally known as EcmaScript
	//ECMA = European Computer Manufactures Association

/*
	MINI-ACTIVITY
	
*/

		let string1 = "Zuitt";
		let string2 = "Coding";
		let string3 = "Bootcamp";
		let string4 = "teaches";
		let string5 = "javascript";

				//Template literals are part of ES6 updates
				// `backticks
				// ${} = placeholder
		console.log(`${string1} ${string2} ${string3} ${string4} ${string5}`);


			// Exponent Operator(**) ES6 Update
			let fivePowerOf2 = 5 ** 2;
			console.log(fivePowerOf2);
						//result : 25

			// Math.pow is the old style
			let x = Math.pow(5,3);
			console.log(x);
					//result : 125

		// TEMPLATE LITERALS WITH JS EXPRESSION
		let sentence2 = `The result of 5 to the power of 2 is ${5**2}`;
		console.log(sentence2);

// ARRAY DESTRUCTURING - ALLOW US TO SAVE ARRAY ITEMS IN A VARIABLE

		let array = ["Kobe", "Lebron", "Shaq", "Westbrook"];

		console.log(array[2]);

		let lakerPlayer1 = array[3];
		let lakerPlayer2 = array[0];

		//ARRAY DESTRUCTURING

		const [kobe, lebron, shaq,] = array;

		console.log(kobe);
				//result : Kobe
		console.log(lebron);
				//result : Lebron
		console.log(shaq);
				//result : Shaq

		/*

		MINI-ACTIVITY - save the items in the array in the following variables
			

		*/
			let array2 = [ "Curry", "Lillard", "Paul", "Irving"];

			const [pointGuard1, pointGuard2, pointGuard3, pointGuard4]= array2;
			console.log(pointGuard1);
			console.log(pointGuard2);
			console.log(pointGuard3);
			console.log(pointGuard4);

			// Lets skip an item (example is Jeremy below)
			let bandMembers = ["Hayley","Zac", "Jeremy", "Taylor"];
			const [vocals, lead, ,bass] = bandMembers;
			console.log(vocals);
			console.log(lead);
			console.log(bass);
			// NOTE = ORDER MATTERS IN DESTRUCTURING
				// Syntax const/let[var1, var2]= array;

// OBJECT DESTRUCTURING
		//destructure an object by adding the values of an object property into respective variables

			let person = {
				name : "Jeremy Davis",
				birthday : "September 2, 1989",
				age : 32
			};

			let sentence3 = `Hi i am ${person.name}`;
			console.log(sentence3);

			const {age, firstName, birthday} = person;
			console.log(age);
					//result : 32
			console.log(firstName);
					//result : undefined because its not a property of person object
			console.log(birthday);
					//result : September 2, 1989

			// NOTE = ORDER DOES NOT MATTER IN OBJECT DESTRUCTURING

			let pokemon1 = {
					name : "Charmander",
					level: 11,
					type : "Fire",
					moves : ["Ember", "Scratch", "Leer"]

			};
			/*MINI ACTIVITY create sentence using object destructuring*/

			const {name, level, type, moves} = pokemon1;
			let sentence4 = `My pokemon is ${name}, it is in level ${level}. It is a ${type} type. It's moves are ${moves}.`
								
			console.log(sentence4);


// ARROW FUNCTIONS 
		//ALTERNATIVE WAY OF WRITING FUNCTIONS 


		//traditional function
		function displayMsg(){
			console.log("Hello World");
		};

		//arrow functiom
		const hello = () => {
			console.log("Hello World Again!")
		};

		displayMsg();
		hello();

		const greet = (person) => {
			console.log(`Hi ${person.name}`)

		};

		greet(person); //from the object we created earlier

		//ARROW FUNC CAN HAVE IMPLICIT RETURN
				// allow us to return value without using the return keyword
				// implicit return only works with a one-liner function
		const addNum = (num1,num2) => num1 + num2;
		let sum = addNum(55,60);
		console.log(sum);


// this keyword
		
		let protagonist = {
			name : "Cloud Strife",
			occupation : "SOLDIER",

			//traditional method would have this keyword refer to the parent object
			greet : function() {
				console.log(this);
				console.log(`Hi I am ${this.name}`)
			},

			// arrow function refer to the global window
			introduce : () => {
				console.log(this);
				console.log(`I work as ${this.occupation}`)
			}

		};
		protagonist.greet();
		protagonist.introduce();


// CLASS-BASED OBJECTS BLUEPRINTS
	// In Js , classes are templates of objects
	// We can create objects out of the use of classes
	// Before the introduction of Classes in Js, we mimic this behaviou

		function Pokemon(name, type, lvl){
			this.name = name;
			this.type = type;
			this.lvl = lvl;
		}

	// ES6 CLASS CREATION

	class Car {
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year =  year;
		}
	}
	let car1 = new Car("Toyota", "Vios", "2002");
	console.log(car1);
	let car2 = new Car("Cooper", "Mini", "1969");
	console.log(car2);
		
		


